from __future__ import unicode_literals

from django.shortcuts import render
from django.db.models import Count
from django.http import HttpResponse, JsonResponse
from feedbackApp.models import Feedback, Product
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime

# Create your views here.

@csrf_exempt
def feedback(request):
	# Post request for posting new feedback entry.
	# Posting feedback does not require login.
	if request.method == "POST":
		# First check that post has all the parameters and inform API user if he/she is missing something.
		if not (u'rating' in request.POST):
			return HttpResponse("Your request did not contain rating.",status=400)
		if not (u'product_id' in request.POST):
			return HttpResponse("Your request did not contain product_id.",status=400)
		# Catch optional comment if user provides one.
		comment = request.POST.get(u'comment',"")
		# get the rating 
		rating = request.POST.get(u'rating')
		# check that rating is ok.
		print (rating)
		if int(rating) < 1 or int(rating) > 5:
			return HttpResponse("Invalid rating",status=400)
		# try to get the product for the feedback from db.
		try:
			product = Product.objects.get(product_id=request.POST.get(u'product_id'))
		except:
			return HttpResponse("Could not find the product with the given id.",status=400)
		try:
			feedback = Feedback(product=product,rating=int(rating),comment=comment)
			feedback.save()
		except:
			HttpResponse("unsuccessful creation of feedback. Some parameters given were invalid.",status=400)
		return HttpResponse("successfully created new feedback.",status=200)
	
	# Get request for querying feedback objects.
	# Does require user to login.
	elif request.method == "GET":
		# if not request.user.is_authenticated():
		# 	return HttpResponse("Please login to query feedbacks.",status=400)
		print (request.GET)
		# Check all the parameters for query
		if "start" in request.GET and "end" in request.GET:
			start = request.GET.get(u'start')
			end = request.GET.get(u'end')
			try:
				start = datetime.strptime(start,'%Y-%m-%d')
				end = datetime.strptime(end,'%Y-%m-%d')
			except:
				return HttpResponse("Invalid date format, should be YYYY-M-D",status=400)
			feedbacks = Feedback.objects.filter(created_at__gte=start,created_at__lte=end).values(u'rating').annotate(no_ratings=Count(u'rating'))
		elif "start" in request.GET:
			start = request.GET.get(u'start')
			try:
				start = datetime.strptime(start,'%Y-%m-%d')
			except:
				return HttpResponse("Invalid date format, should be YYYY-M-D",status=400)
			feedbacks = Feedback.objects.filter(created_at__gte=start).values(u'rating').annotate(no_ratings=Count(u'rating'))
		elif "end" in request.GET:
			end = request.GET.get(u'end')		
			try:
				end = datetime.strptime(end,'%Y-%m-%d')	
			except:
				return HttpResponse("Invalid date format, should be YYYY-M-D",status=400)
			feedbacks = Feedback.objects.filter(created_at__lte=end).values(u'rating').annotate(no_ratings=Count(u'rating'))
		else:
			# Return all feedbacks.
			feedbacks = Feedback.objects.all()
		return JsonResponse({'feedbacks':list(feedbacks)})			