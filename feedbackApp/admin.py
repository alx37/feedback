from django.contrib import admin
from feedbackApp.models import Feedback, Product

# Register your models here.
admin.site.register(Feedback)
admin.site.register(Product)